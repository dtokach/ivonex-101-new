//svg
$(document).ready(function () {
    let cloud1 = $('.index-top__cloud1');
    let cloud2 = $('.index-top__cloud2');
    let cloud3 = $('.index-top__cloud3');
    let taxi = $('.index-top__taxi');
    let train = $('.index-top__train');

    function timer() {
        cloud1.animate({left: "-=28.5%"}, {

            duration: 10000,
            specialEasing: {
                left: 'linear',
            }
        });
        cloud1.animate({opacity: "0"}, 1000);
        cloud1.animate({left: "+=28.5%"}, 0);
        cloud1.animate({opacity: "1"}, 1000);
        cloud2.animate({left: "-=54.5%"}, {

            duration: 20000,
            specialEasing: {
                left: 'linear',
            }
        });
        cloud2.animate({opacity: "0"}, 1000);
        cloud2.animate({left: "+=54.5%"}, 0);
        cloud2.animate({opacity: "1"}, 1000);
        cloud3.animate({left: "-=78.5%"}, {

            duration: 13000,
            specialEasing: {
                left: 'linear',
            }
        });
        cloud3.animate({opacity: "0"}, 1000);
        cloud3.animate({left: "+=78.5%"}, 0);
        cloud3.animate({opacity: "1"}, 1000);


        taxi.animate({left: "+=84.5%"}, {

            duration: 12000,
            specialEasing: {
                left: 'linear',
            }
        });
        taxi.animate({opacity: "0"}, 1000);
        taxi.animate({left: "-=84.5%"}, 0);
        taxi.animate({opacity: "1"}, 1000);

        train.animate({left: "-=27.5%"}, {

            duration: 14000,
            specialEasing: {
                left: 'linear'
            }
        });
        train.animate({opacity: "0"}, 1000);
        train.animate({left: "+=27.5%"}, 0);
        train.animate({opacity: "1"}, 1000);
        window.setTimeout(timer, 1000);
    }

    timer();
});

$('#copy_ref').click(function () {
    var el = ($(this).html()).trim();
    new ClipboardJS('#copy_ref');
});

/* Ресайз карты на странице support */

/*if($(window).width() >= 992) {
    var height = $(document).height() - $('header').height() - $('.block').height() - $('footer').height();
    var width = (($('body').width() - 1200)/2)+(1200 * 0.5) - 15;
    $('.map').css({ 'height': height, 'width': width });
} else {
    $('.support .map').each(function(i, e) {
        var w_post_img = $(e).width();
        var h_post_img = (w_post_img * 3) / 4;
        $(e).css('height', h_post_img);
    });
}
$(window).resize(function() {
    if($(window).width() >= 992) {
        var height = $(document).height() - $('header').height() - $('.block').height() - $('footer').height();
        var width = (($('body').width() - 1200)/2)+(1200 * 0.5) - 15;
        $('.map').css({ 'height': height, 'width': width });
    } else {
        $('.support .map').each(function(i, e) {
            var w_post_img = $(e).width();
            var h_post_img = (w_post_img * 3) / 4;
            $(e).css('height', h_post_img);
        });
    }
});

$('.invest_tabs .tab').click(function() {
    $('.invest_tabs .tab').removeClass('active');
    $(this).addClass('active');
    var id = $(this).attr('id');
    $('.inv').fadeOut( 0, function() { });
    $('.inv.id_' + id).fadeIn( 600, function() { });
});*/

/* Копирование ссылки при клике на кнопку */

/*$('#copybtn').click(function () {
    copy();
});

function copy() {
    var cutTextarea = document.querySelector('#copy');
    cutTextarea.select();
    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copy');
    } catch (err) {
        console.log('Oops, unable to cut');
    }
}*/

/* Инициализация WOW эффектов */

new WOW().init();

/* Создание калькулятора */

$(document).ready(function () {
    qwer();
});

$("input.dataInput").on('input', function () {
    di = $(this).val();
    parseFloat(di);
    if (document.getElementsByClassName('range01').length > 0) {
        $(".range01").slider("option", "values", [0, di]);
    }

    if (di >= +summin && di < sumsecond) {
        percent = percentfirst;
    } else if (di >= +sumsecond && di <= sumthird) {
        percent = percentsecond;
    } else if (di >= +sumthird && di < summax) {
        percent = percentthird;
    } else {
        percent = percentlast;
    }

    qwer();
});

$("input.daysInput").on('input', function () {
    days = $(this).val();
    parseInt(days);

    if (di >= +summin && di < sumsecond) {
        percent = percentfirst;
    } else if (di >= +sumsecond && di <= sumthird) {
        percent = percentsecond;
    } else if (di >= +sumthird && di < summax) {
        percent = percentthird;
    } else {
        percent = percentlast;
    }

    qwer();
});

function qwer() {
    di = $('input.dataInput').val();
    days = $('input.daysInput').val();

    var per        = percent * days;
    profit     = di * per / 100;
    hashpower  = profit * rate;

    $('.percent').html(per.toFixed(2) + '% of investment');
    $('.profit').html(+profit.toFixed(2) + ' USD');
    $('.hashpower').html(+hashpower.toFixed(2) + ' GH/s');
    $('.days').html(+days);
};

if (document.getElementsByClassName('range01').length > 0) { // те такие блоки есть
    function range(min, max) {
        $(".range01").slider({
            range: true,
            min: min,
            max: max,
            disabled: true
        });
    }
    range(summin, summax);
}